package com.zuitt.batch193;

public class Driver {
    // this is property
    private String name;
    //this is empty constructor object
    public Driver(){};
    //parameterized constructor
    public Driver(String name){
        this.name = name;
    }
    //this is getters
    public String getName(){
        return this.name;
    }
    //this is setters
    public String setName(){
        return this.name = name;
    }

}
