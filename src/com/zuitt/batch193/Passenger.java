package com.zuitt.batch193;

public class Passenger {

    // this is property
    private String passengerName;
    // this is constructor
    public Passenger(String passengerName){
        this.passengerName = passengerName;
    }
    // this getter
    public String getPassengerName(){
        return this.passengerName;
    }
}
