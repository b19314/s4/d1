package com.zuitt.batch193;

public class Car {

// blueprint of out Car object
// Class Creation - is composed of four parts:
// 1. Properties - characteristics of the object
// 2. Constructors - used to create an object
// 3. Getters / Setters - get and set teh values of each property of the object
// 4.  Methods - functions of an object where it can perform a task

// Access Modifiers
//   1. Default - no keyword required.. only those classes that are in the same package can access this class. No other class outside the package can access the class
//   2. Private - properties and methods are only accessible within the class
//   3. Public - the member, methods, and classes that are declared public can be access from anywhere.
//   4. Protected - accessible member only or allowed to get methods properties in modifiers similar on default but selected modifiers only

    // Properties = qualities or characters of real world objects()

    // this is PROPERTIES
    public String name;
    private String brand;
    private int manufactureDate;
    private String owner;
    //make Driver component
    private Driver d;
    private Passenger p;

    // Constructors
    // empty constructor - it is a common practice to create an empty and parametized consctructor for creating new instances

    // this is empty constructor
    public Car(){
        //add in curly braces a driver so whenever a new car is created, it will always have a driver
        this.d = new Driver("Alejandro");
        //whenever a new car is created, it will always "HAS" have a passenger
        this.p = new Passenger("Lady Gaga");
    };


    // parameterized constructor = accepts properties parameters
    public Car(String name, String brand, int manufactureDate, String owner){
        this.name = name;
        this.brand = brand;
        this.manufactureDate = manufactureDate;
        this.owner = owner;
    }

    // Getters and Setters
    // This is used for retrieving and changing the properties(write-only, read-only)

    //add a getter for driver new
    public String getDriverName(){
        return this.d.getName();
    }

    //add a getter for driver new
    public String getPassengerName(){
        return this.p.getPassengerName();
    }

    //GETTERS - read-only or just getting the

    //this is ENCAPSULATION
    // you can achieve data hiding
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getManufactureDate(){
        return this.manufactureDate;
    }
    public String getOwner(){
        return this.owner;
    }

    //SETTERS - write-only or for setting up the properties
    // using void because no need to return
    public void setName(String name){
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setManufactureDate(int manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    //METHODS using void because no need to return
    public void drive(){
        System.out.println(getOwner() + " drives the " + getBrand());
    }

    //
    public void printDetails(){
        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by " + getOwner());
    }

}
