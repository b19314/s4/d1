package com.zuitt.batch193;

public class Animal {
    //PARENT CLASS
    //properties
    //constructor
    private String name;
    private String color;

    //this is empty constructor object
    public Animal(){}

    //parameterized constructor
    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    //getter
    public String getName(){
        return name;
    }

    public String getColor(){
        return color;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //method
    public void call(){
        System.out.println("Hi my name is " + getName());
    }
}
