package com.zuitt.batch193;

public class Person implements Actions {
    //to be able for class to subscribe to an interface, it should be able to "implement" that interface

    public void sleep(){
        System.out.println("Zzzzzz....");
    }

    public void run() {
        System.out.println("Running");
    }

    public void morningGreet() {
        System.out.println("Good Morning, Friend");
    }

    public void holidayGreet() {
        System.out.println("Happy Holidays,, Friend");
    }
}
