package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){
    // Installation of object
    // new instances with an empty constructor
        // this is SETTERS
        Car myFirstCar = new Car();
        myFirstCar.setName("Civic");
        myFirstCar.setBrand("Honda");
        myFirstCar.setManufactureDate(1998);
        myFirstCar.setOwner("John");

        System.out.println(myFirstCar.getName());
        System.out.println(myFirstCar.getBrand());
        System.out.println(myFirstCar.getManufactureDate());
        System.out.println(myFirstCar.getOwner());

    // new instance with parameterized parameter
        // this is GETTERS
        Car mySecondCar = new Car("Charger", "Dodge", 1978, "Vin Diesel");

        // we used the getters from the Car class to retrieve the property of the object
        System.out.println(mySecondCar.getName());
        System.out.println(mySecondCar.getBrand());
        System.out.println(mySecondCar.getManufactureDate());
        System.out.println(mySecondCar.getOwner());

        // this is METHODS
        myFirstCar.drive();
        mySecondCar.drive();

        myFirstCar.printDetails();
        mySecondCar.printDetails();

        // Encapsulation - we bundled each fields and methods inside a single class and we used access modifiers like public to allow the access from another class

        // why we use encapsulation
        // The fields of a class can be made read-only and write-only
        // a class can have a total control over what is stored in its field
        // In Java, encapsulation helps us to keep related fields and methods together which make our code cleaner and easy to read.
        // We can achieve data hiding using thse access modifier private

        Car anotherCar = new Car();
        // setName is to set the name you want to print and get methods

        anotherCar.setName("Ferrari");
        System.out.println(anotherCar.getName());

        // java: name has private access in com.zuitt.batch193.Car because is a private access
        anotherCar.name = "Honda";
        System.out.println(anotherCar.getName());

        // COMPOSITION = Ex. A car has a driver
                // allows modeling objects that are made up of other objects. It defines "HAS a relationship"
        Car newCar = new Car();
        System.out.println("This car is driven by " + newCar.getDriverName());
        System.out.println("A car has a driver " + anotherCar.getDriverName());

        // everytime we added a new Car object, the Car() always HAS a driver and a passenger
        System.out.println("This car HAS a paseenger name " + newCar.getPassengerName());

        //Inheritance = Ex. A car is a vehicle
        //allows modelling objects inherit as a subject of another objects. It defines "IS a relationship"
        //Inheritance can be defined as the process where one class acquires the properties (methods and fields) of another class.

        //"extends" is the keyword used to inherit the properties of a class
        //"super" keyword used for referencing the variable, properties or method which can be used to another class

        Dog dog1 = new Dog();
        System.out.println(dog1.getBreed());

        dog1.setName("Brownie");
        dog1.setColor("Brown");

        System.out.println(dog1.getName());
        System.out.println(dog1.getColor());

        Dog doge = new Dog("Blackie", "Black", "aspin");

        System.out.println(doge.getBreed());

        //method
        doge.call();
        doge.speak();

        // INTERFACES
        // is use to achieve the abstraction
        // In general terms, an interface can be defined as a container that stores the signature of methods tot be implemented in the code segment

        Person jane = new Person();
        jane.run();
        jane.sleep();

        jane.morningGreet();
        jane.holidayGreet();

        //POLYMORPHISM
        // - the ability of an object to take on many forms
        // - there are two main types
            // 1. staticc or compile polymorphism
            // 2. is usually done by function/method overloading

        // 1. Static or Compile polymorphism
        StaticPoly print = new StaticPoly();
        System.out.println(print.addition(5, 5));
        System.out.println(print.addition(5, 5, 5));
        System.out.println(print.addition(5.6, 5.8));

        // 2. Dynamic Method Dispatch or Runtime Polymorphism
        // is usually done by function/method overloading.

        Parent parent1 = new Parent();
        parent1.speak(); // method of super class or parent class

        //Overriding Parent
        Parent subObject = new Child(); // its called upcasting if you have put override
        subObject.speak(); // method of sub class or child class is called by Parent reference, this process called "run time POLYMORPHISM"

    }
}
